
ChatApp = require('./chatapp').ChatApp;
handlers = require('./handlers');

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');



webinarChat.on('message', handlers.chatOnMessage);
facebookChat.on('message', handlers.chatOnMessage);

vkChat.on('message', handlers.chatOnMessage);

module.exports = {
  webinarChat,
  facebookChat,
  vkChat
}
