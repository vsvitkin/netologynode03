/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 3
*/


handlers = require('./handlers');
chats = require('./chat');

chats.vkChat.setMaxListeners(2).on('message',handlers.preparingToAnswer).on('close',()=>{
   console.log('Чат вконтакте закрылся :(');
 }).close();
chats.webinarChat.on('message', handlers.preparingToAnswer);



// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
chats.vkChat.removeListener('message', handlers.chatOnMessage);
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
chats.facebookChat.removeListener('message', handlers.chatOnMessage);
}, 15000 );

// Отписать chatOnMessage от вебинара webinarChat
setTimeout( ()=> {
  console.log('Прошло 30 секунд, отписываем chatOnMessage от вебинара!');
chats.webinarChat.removeListener('message', handlers.chatOnMessage);
}, 30000 );
